# encoding: UTF-8

# Cookbook Name:: gitlab_fluentd
# Recipe:: prometheus
# License:: MIT
#
# Copyright 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'
include_recipe 'gitlab_fluentd::monitoring'
