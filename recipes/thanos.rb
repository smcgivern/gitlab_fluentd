# encoding: UTF-8

# Cookbook Name:: gitlab_fluentd
# Recipe:: thanos
# License:: MIT
#
# Copyright 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'
include_recipe 'gitlab_fluentd::monitoring'
