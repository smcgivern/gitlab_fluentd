# encoding: UTF-8

# Cookbook Name:: gitlab_fluentd
# Recipe:: consul_cluster
# License:: MIT
#
# Copyright 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

execute 'install multi-format gem' do
  command '/usr/sbin/td-agent-gem install fluent-plugin-systemd'
  notifies :restart, 'service[td-agent]', :delayed
  not_if { ::Dir['/opt/td-agent/embedded/lib/ruby/gems/*/gems/fluent-plugin-systemd-*'].any? }
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'consul_cluster.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
