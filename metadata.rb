# encoding: UTF-8

name             'gitlab_fluentd'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'FluentD log agent cookbook'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.14.2'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_fluentd/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_fluentd'

supports 'ubuntu', '= 16.04'

depends 'gitlab_secrets'
depends 'logrotate', '~> 2.2.0'
depends 'managed_directory'
