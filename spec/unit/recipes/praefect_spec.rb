require 'spec_helper'

describe 'gitlab_fluentd::praefect' do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('elastic_cloud_host' => '1.2.3.4',
                  'elastic_cloud_port' => '1234',
                  'elastic_cloud_user' => 'root',
                  'elastic_cloud_password' => 'toor',
                  'pubsub_key' => '{"foo": "bar"}')
    stub_command(fluent_plugin_google_cloud_command).and_return(0)
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_env'] = 'prd'
        node.normal['gitlab_fluentd']['pubsub_project'] = 'production'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'renders the praefect config correctly' do
      expect(chef_run).to(render_file('/etc/td-agent/conf.d/praefect.conf').with_content do |content|
        expect(content).to eq(IO.read('spec/fixtures/praefect.template'))
      end)
    end
  end
end
