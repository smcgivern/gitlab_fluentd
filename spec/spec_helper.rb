# encoding: UTF-8

require 'chefspec'
require 'chefspec/berkshelf'

module Gitlab
  module FluentSpecHelper
    def fluent_plugin_google_cloud_command
      "/usr/sbin/td-agent-gem list '^fluent-plugin-google-cloud$' -i -v 0.7.29"
    end
  end
end

RSpec.configure do |config|
  config.include Gitlab::FluentSpecHelper
end

at_exit { ChefSpec::Coverage.report! }
